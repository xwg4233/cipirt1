//app.js
const api = require('/api.js');
App({

  /**
   * 当小程序初始化完成时，会触发 onLaunch（全局只触发一次）
   */
  onLaunch: function () {
    const updateManager = wx.getUpdateManager()

    updateManager.onCheckForUpdate(function (res) {
      // 请求完新版本信息的回调
      console.log(res.hasUpdate)
    })

    updateManager.onUpdateReady(function () {
      wx.showModal({
        title: '更新提示',
        content: '新版本已经准备好，是否马上重启小程序？',
        success: function (res) {
          if (res.confirm) {
            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
            updateManager.applyUpdate()
          }
        }
      })
    })
    updateManager.onUpdateFailed(function () {
      // 新的版本下载失败
    })
    console.log(wx.getStorageSync('openId'));
    if (wx.getStorageSync('openId') == "" || wx.getStorageSync('openId') == null || wx.getStorageSync('openId') == undefined || wx.getStorageSync('openId').length <=5){
      console.log("登录")
      this.login()
      return
    }
    this.zhucheng();
  },
  zhucheng:function(){
    wx.request({
      url:api.getOpenIdLogin(),
      data: {
        "OpenId": wx.getStorageSync('openId'),
        "Key":'15B4B190-4CE4-4885-A7E8-AF5CB8727A9C'
      },
      success(res) {
        console.log(res)
        var code=false;
        console.log(res.data.code);
        if (res.data.code=='0'){
          code=false;
        
        }else{
          code=true
        }
        console.log(code);
        wx.setStorageSync('code', code);
      }, fail() {

      }
    })
  
  },
  login(){
    var thas=this;
    wx.login({
      success: function (res) {
        console.log(res);
        if(res.code){
          wx.request({
            url: api.login(),
            data: { code: res.code },
            header: {
              'content-type': 'application/json' //默认值
            },
            success: function (res) {
              console.log(res)
              console.log(res.data);
              wx.setStorageSync('openId', res.data);
              thas.zhucheng();
            }
          })
        }
       },
      fail: function (res) {
        wx.showToast({
          title: res.msg,
        })
       },
      complete: function (res) { },
    })
  },
  /**
   * 当小程序启动，或从后台进入前台显示，会触发 onShow
   */
  onShow: function (options) {
  },
  openId:function(openId){
    var value = wx.getStorageSync('code');
    if (value==false){
      wx.navigateTo({
        url: '../logs/logs',
      })
      return
    }
  
  },
  imgshowloding() {
    wx.showLoading({
      title: '加载中...',
    })

  },
  deleloading() {
    wx.hideLoading()
  },
  /**
   * 当小程序从前台进入后台，会触发 onHide
   */
  onHide: function () {

  },
  
  request:function(){
    
  },

  /**
   * 当小程序发生脚本错误，或者 api 调用失败时，会触发 onError 并带上错误信息
   */
  onError: function (msg) {
    
  },
 globalData:{

 },

})
