var host ="https://edentech.corpit.com.cn/";
var hostHttps = 'https://mdentech.corpit.com.cn/';
var hostpps ='https://dentech.corpit.com.cn/';
//添加意见接口：
function _AddOpinionList() {
  return hostpps + 'GetInfo/AddOpinionList';
}
//查询意见类别接口
function _GetOpinionType() {
  return hostpps + 'GetInfo/GetOpinionType';
}
//展商验证

function _WxECheck() {
  return host + 'WxECheck.aspx';
}
//收藏查询接口

function _GetSearchCollectionList() {
  return hostpps + 'GetInfo/GetSearchCollectionList';
}
//查询已注册
function _GetOpenIdLogin() {
  return hostpps + 'GetInfo/GetOpenIdLogin';
}
//邀请函
function _wxCreateQRcode() {
  return host + 'wxCreateQRcode.aspx';
}
//登录

function _login() {
  return host + 'checkid.aspx';
}
//查询是否收藏接口
function _GetOpenIdCollection() {
  return hostpps + 'GetInfo/GetOpenIdCollection';
}

//取消收藏接口
function _DelCollectionList() {
  return hostpps + 'GetInfo/DelCollectionList';
}
//添加收藏接口
function _AddCollectionList() {
  return hostpps + 'GetInfo/AddCollectionList';
}
//查询收藏接口
function _GetCollectionList() {
  return hostpps + 'GetInfo/GetCollectionList';
}
//展团详情
function _GetPavilionDetails() {
  return hostpps + 'GetInfo/GetPavilionDetails';
}
function _getUseryzm(){
  return hostpps + 'GetInfo/GetUseryzm';
}
function _getLogin() {
  return hostpps + 'GetInfo/GetLogin';
}
function _wxconfig(){
  return host + 'wxconfig.aspx';
}
function _wxnote() {
  return host + 'wxnote.aspx';
}
function _wxQuickNews() {
  return host + 'wxQuickNews.aspx';
}
// 展商所属类别
function _wxpcate() {
  return host + 'wxpcate.aspx';
}
//展馆列表
function _wxelist() {
  return host + 'wxelist.aspx';
}
//展商详情

function _WxEshow(){
  return host + 'wxeshow.aspx';
}
function _CategoryDetails() {
  return hostHttps + 'GetCategory/CategoryDetails';
}
function _GetTable() {
  return hostpps + 'GetInfo/GetTable';
}
//展团图片接口
function _GetPavilion() {
  return hostpps + 'GetInfo/GetPavilion';
}
function _GetTypeDetails() {
  return hostpps + 'GetInfo/GetTypeDetails';
}

function _search() {
  return host + 'search.aspx';
}

function _WxGetBoothMap() {
  return host + 'WxGetBoothMap.aspx';
}


module.exports={
  wxconfig: _wxconfig,
  wxnote: _wxnote,
  wxQuickNews:_wxQuickNews,
  wxpcate: _wxpcate,
  wxelist: _wxelist,
  wxEshow: _WxEshow,
  categoryDetails: _CategoryDetails,
  host: host,
  getUseryzm: _getUseryzm,
  getLogin: _getLogin,
  getTable: _GetTable,
  getCollectionList: _GetCollectionList,
  addCollectionList: _AddCollectionList,
  getOpenIdCollection: _GetOpenIdCollection,
  delCollectionList: _DelCollectionList,
  login: _login,
  wxCreateQRcode: _wxCreateQRcode,
  getOpenIdLogin: _GetOpenIdLogin,
  getSearchCollectionList: _GetSearchCollectionList,
  wxECheck: _WxECheck,
  getOpinionType: _GetOpinionType,
  addOpinionList: _AddOpinionList,
  getPavilion: _GetPavilion,
  getTypeDetails: _GetTypeDetails,
  search: _search,
  wxGetBoothMap: _WxGetBoothMap,
  getPavilionDetails: _GetPavilionDetails
}