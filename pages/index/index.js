//index.js
//获取应用实例
const app = getApp()
const util = require('../../utils/util.js');
const api =require('../../api.js');
Page({
  /**
   * 页面的初始数据
   */
  data: {
    tempFilePath:'',
    indexhuabu: 0,
    showid: [{ id: 'canvasId0' }],
    silt:[],
    advertisingshow:0,
    timeArrt:[
      {time:"0天"},
      { time: "0时"},
      { time: "0分"},
      {time:"0秒"},
    ],
    indexHeaderIcoarrt:[

    ],
    indexHeaderIco:"",
    notice:[],
    navLsit:[
        {
        "icon":"/img/exhibition.png",
          "content":"展会信息"

        },
      {
        "icon": "/img/positioning.png",
        "content": "展馆定位"

      },
      {
        "icon": "/img/invitation.png",
        "content": "定制邀请"

      },
      {
        "icon": "/img/exhibits.png",
        "content": "展商展品"

      },
    
      {
        "icon": "/img/pavilion.png",
        "content": "官方展团"

      },
      {
        "icon": "/img/activity1.png",
        "content": "论坛活动"

      },
      {
        "icon": "/img/service1.png",
        "content": "商旅服务"

      },
      {
        "icon": "/img/personal.png",
        "content": "个人中心"

      },
        


    ],
    activitySilt:[
      {
        icon:"/img/activityIcon.jpg",
        content:'口腔医生必须关注的口腔全局美学新趋势——口腔微整形',
        time: "2019-10-18",
      }, {
        icon: "/img/activityIcon.jpg",
        content: '口腔医生必须关注的口腔全局美学新趋势——口腔微整形',
        time: "2019-10-18",
      },
      {
        icon: "/img/activityIcon.jpg",
        content: '口腔医生必须关注的口腔全局美学新趋势——口腔微整形',
        time:"2019-10-18",
      }
    ],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {




    this.setData({
      yearTime: util.dateTime().yearTime,
      getDayTime: util.dateTime().getDayTime
    })

    console.log(util.dateTime());
    
    var thas=this;
  
    wx.request({
      url: api.wxconfig(),
      data: {},
      success(res) {
   console.log(res);
        thas.data.silt.push('https://edentech.corpit.com.cn/WxFile/banner1.jpg', 'https://edentech.corpit.com.cn/WxFile/banner2.jpg');
        console.log(thas.data.silt);
        console.log(res.data[0].Banner);
        var dateTime = new Date(res.data[0].ShowTime);
        thas.setData({
          indexHeaderIcoarrt: thas.data.silt,
          dateTime: dateTime,
        })



        console.log(util.getTimeLeft(dateTime));
        var time = util.getTimeLeft(dateTime)
       setInterval(() => {
          thas.setData({
            timeArrt: util.getTimeLeft(dateTime)//使用了util.getTimeLeft
          });
          if (dateTime == "0天0时0分0秒") {
          }
        }, 1000);

      },
    })
    wx.request({
      url: api.wxnote(),
      data: {},
      success(res) {
        console.log(res.data);
        thas.setData({
          notice:res.data,
        })
      }
    });
    wx.request({
      url: api.wxQuickNews(),
      data:{},
      success(res){
        console.log(res)
        thas.setData({
          activitySilt:res.data,
        })
      },fail(res){
console.log(res);
      }
    })


    
    
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    app.zhucheng();

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },
  navPages(e){
console.log(e);
    switch (e.currentTarget.dataset.item){
      case 0:
      wx.navigateTo({
        url: '../information/information',
      })

      break;
      case 1:
      wx.navigateTo({
        url: '../positioning/positioning',
      })
      break;

    case 2:
        wx.navigateTo({
          url: '../invitation/invitation',
        })
    break;
    case 3:
  wx.navigateTo({
    url: '../exhibits/exhibits',
  })

    break;
    case 4:
    wx.navigateTo({
      url: '../pavilion/pavilion',
    })
    break;
      case 5:
      wx.navigateTo({
        url: '../activity/activity',
      })
        break;
      case 6:
      wx.navigateTo({
        url: '../service/service',
      })

        break;
        case 7:
       var url = '../personalCenter/personalCenter';
        if (wx.getStorageSync('code')==false){
          app.openId();
        }else{
          wx.navigateTo({
            url: url,
          })
        }
      
    break;
}
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
  activity(){
    wx.navigateTo({
      url: '../activeContent/activeContent',
    })
  }
})
