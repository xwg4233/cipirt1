// pages/information/information.js
const app=getApp();
const api = require('../../api.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgico:'',
    img:'',

    attr: [{
      icoChose: "/img/meetingDistributionChose.png",
      ico: "/img/meetingDistribution.png",
      text: "官方展团介绍",

    }, {
        icoChose: "/img/minglu-chose.png",
        ico: "/img/minglu.png",
      text: "展团公司名录",


    }],
    ifShow: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    app.imgshowloding();
    var thas=this;
    var img='';
    var imgico='';
    console.log(Number(options.index));
    wx.request({
      url: api.getPavilionDetails(),
      data:{
        PavilionId: options.index,
        Key:'15B4B190-4CE4-4885-A7E8-AF5CB8727A9C',
      },
      success(res){
        wx.hideLoading()
        console.log(res.data);
        thas.setData({
          dataUrl:res.data,
        }) 
      },fail(){
        wx.hideLoading()
      }
    })
//     switch (Number(options.index)){
//       case 0:
//         img ="https://edentech.corpit.com.cn/wxfile/德国国家展团.jpg";
//         imgico = "https://edentech.corpit.com.cn/wxfile/德国名录.jpg"

// break;
// case 1:
//         img = "https://edentech.corpit.com.cn/wxfile/美国展团.jpg";
//         imgico = "https://edentech.corpit.com.cn/wxfile/美国国家展团名录.jpg"
// break;

// case 2:
//         img = "https://edentech.corpit.com.cn/wxfile/韩国展团.jpg";
//         imgico = "https://edentech.corpit.com.cn/wxfile/韩国展团名录.jpg"

// break;
//       case 3:
//         img = "https://edentech.corpit.com.cn/wxfile/意大利展团.jpg";
//         imgico = "https://edentech.corpit.com.cn/wxfile/意大利展团名录.jpg"

//         break;
//       case 4:
//         img = "https://edentech.corpit.com.cn/wxfile/台湾展团.jpg";
//         imgico = "https://edentech.corpit.com.cn/wxfile/台湾展团名录.jpg"

//         break;

//       case 5:
//         img = "https://edentech.corpit.com.cn/wxfile/佛山展团.jpg";
//         imgico = "https://edentech.corpit.com.cn/wxfile/佛山展团名录.jpg"

//         break;
//     }
  
  
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  bindload(){
    app.deleloading();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  indexNav(e) {
    console.log(e.currentTarget.dataset.index);
    this.setData({
      ifShow: e.currentTarget.dataset.index,
    })
    var list="";
    if (e.currentTarget.dataset.index==0){

      list= "官方展团介绍";
    }else{
      list ='展团公司名录'
    }
    wx.setNavigationBarTitle({
      title: list,
    })


  },
  navList() {
    if (wx.getStorageSync('code') == false) {
      app.openId();
    } else {
      wx.navigateTo({
        url: "../ticket/ticket",
      })
    }
  }
})