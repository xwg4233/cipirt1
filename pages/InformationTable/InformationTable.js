// pages/InformationTable/InformationTable.js
const api = require('../../api.js');
const app=getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    checkboxChange:[],
    show:0,
    animationData:[],
    index:0,
    indexrangeProvinces:0,
    indexregion:0,
    phone:'',
    animationCheckbox:[],
    items: [
      { name: 'P117', value: '灭菌消毒和感染控制类产品', checked: '' },
      { name: 'P107', value: '牙科技工室家具及设备', checked: '' },
      { name: 'P103', value: '牙科诊室家具及设备', checked: '' },
      { name: 'P111', value: '义齿加工所', checked: ''},
      { name: 'P104', value: '仪器和工具', checked: ''},
      { name: 'P113', value: '牙科技工室所用材料', checked: ''},
      { name: 'P116', value: '牙科诊室所用材料', checked: '' },
      { name: 'P120', value: '预防及其他', checked: '' },
      { name: 'P106', value: '其他用于牙科技工室的附件和辅助材料', checked: '' },
      { name: 'P114', value: '其他用于牙科诊室的附件和辅助材料', checked: '' },
      { name: 'P118', value: '义齿修复材料和种植产品及其附件', checked: '' },
      { name: 'P102', value: '正畸产品及其附件', checked: '' },
    ],
    rangeProvinces:[
      {
        name: "上海",
        value: '21'


      },
      {
        name:"北京",
        value:'10'
      
        },
      {
        name: "天津",
        value: '22'


      },
      {
        name: "重庆",
        value: '23'


      },
      {
        name: "河北",
        value: '311'


      },
      {
        name: "陕西",
        value: '29'


      }, {
        name: "内蒙古",
        value: '471'


      },
      {
        name: "辽宁",
        value: '24'


      },
      {
        name: "吉林",
        value: '431'


      },
      {
        name: "黑龙江",
        value: '451'


      },
      {
        name: "江苏",
        value: '25'


      },
      {
        name: "浙江",
        value: '571'


      },
      {
        name: "安徽",
        value: '551'


      },
      {
        name: "福建",
        value: '591'


      },
      {
        name: "江西",
        value: '792'


      },
      {
        name: "山东",
        value: '531'


      },
      {
        name: "河南",
        value: '371'


      },
      {
        name: "湖北",
        value: '27'


      },
      {
        name: "湖南",
        value: '731'


      },
      {
        name: "广东",
        value: '20'


      },
      {
        name: "广西",
        value: '771'


      },
      {
        name: "海南",
        value: '898'


      },
      {
        name: "四川",
        value: '28'


      },
      {
        name: "贵州",
        value: '851'


      },
      {
        name: "云南",
        value: '871'


      },
      {
        name: "西藏",
        value: '891'


      },
      {
        name: "山西",
        value: '351'


      },
      {
        name: "甘肃",
        value: '931'


      },
      {
        name: "青海",
        value: '971'


      },
      {
        name: "宁夏",
        value: '951'


      },
      {
        name: "新疆",
        value: '991'


      },

    ],
    range:[
      { name: "医师", value:'01'},
      { name: "技师", value: '02' },
      { name: "助理(咨询师)", value: '03'},
      { name: "管理", value: '04'},
      { name: "采购", value: '05'},
      { name: "销售", value: '06'},
      { name: "市场", value: '07'},
      { name: "研发", value: '08'},
      { name: "教师", value: '09'},
      { name: "学生", value: '10'},
      { name: "其他", value: '11'},
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  

    this.setData({
      phone:options.phone,
    })
    

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  },
  animationData(){
    var show='';
    var animationCheckbox = wx.createAnimation({
      duration: 300,
      timingFunction: 'ease',
    })
    var animation = wx.createAnimation({
      duration: 300,
      timingFunction: 'ease',
    })
    if(this.data.show==0){
      show=1
      animation.rotate(90).step();
      animationCheckbox.opacity(1).step();
    }else{
      show = 0
      animation.rotate(0).step();
      animationCheckbox.opacity(0).step();
    }
  
    this.setData({
      animationData: animation.export(),
      animationCheckbox: animationCheckbox,
  
    })
    setTimeout(() => {

      this.setData({
        show: show
      })

    }, 300)
    // if (show == 1) {
    
    // } else {
    //   this.setData({
    //     show: show
    //   })
    // }
    
  
  },


  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  submitForm(e){
    var value= e.detail.value;
    var thas=this;
//姓氏
    if (value.usersurnames == null || value.usersurnames == undefined || value.usersurnames ==""){
      wx.showToast({
        title: '请输入姓氏',
        icon:'none'
      })
      return
    }
    

    if (value.userName == null || value.userName == undefined || value.userName == "") {
      wx.showToast({
        title: '请输入名称',
        icon: 'none'
      })
      return
    }

    if (value.unit == null || value.unit == undefined || value.unit == "") {
      wx.showToast({
        title: '请输入单位名称',
        icon: 'none'
      })
      return
    }
    if (value.city == null || value.city == undefined || value.city == "") {
      wx.showToast({
        title: '请输入城市地址',
        icon: 'none'
      })
      return
    }
    if (value.address == null || value.address == undefined || value.address == "") {
      wx.showToast({
        title: '请输入详细地址',
        icon: 'none'
      })
      return
    }
    
    //手机
      var myreg = /^[1][3,4,5,7,8][0-9]{9}$/;
      if (!myreg.test(value.phone)) {
       wx.showToast({
         title: '请输入正确手机号',
       })
       return
      } 
//邮箱
    const reg = /^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/;
    if (!reg.test(value.email)) {
      wx.showToast({
        title: '请输入正确邮箱',
        icon: 'none',
      })
      return
    } 
    let checkboxChange = thas.data.checkboxChange.join("|");
    console.log(checkboxChange);
    if (thas.data.checkboxChange == null || thas.data.checkboxChange == undefined || thas.data.checkboxChange==""){
      wx.showToast({
        title: '请选择您感兴趣的产品',
        icon: 'none',
      })
      return
    }
    wx.showToast({
      title: '提交中...',
      icon:'none',
    })
    wx.request({
      url: api.getTable(),
      data:{
        'OpenId': wx.getStorageSync('openId'),
        'FirstName': value.usersurnames,
        'Key':'15B4B190-4CE4-4885-A7E8-AF5CB8727A9C',
        'LastName': value.userName,
        'Company': value.unit,
        'JobCatagory': thas.data.range[thas.data.index].value,
        'Province': thas.data.rangeProvinces[thas.data.indexrangeProvinces].value,
       'City': value.city,
        'Address': value.address,
        'Mobilephone':value.phone,
        'Email': value.email,
        'Ques': checkboxChange,

      },
      success(e){
        console.log(e);
        if(e.data.code==0){
          wx.showToast({
            title: e.data.msg,
            icon:'none',
          })
          app.zhucheng();
          return
        }
        wx.showToast({
          title: e.data.msg,
          icon: 'none',
          success(){
            setTimeout(function(){
              wx.navigateBack({
                delta: 2,
              },3000)
            })
          
          }
        })
        wx.setStorageSync('Url', e.data.data.Url);
      }
    })
    
  },

  rangeProvinces(e){
    console.log(e)
    this.setData({
      indexrangeProvinces:e.detail.value,
    })

  },
  checkboxChange(e){
    console.log(e);
    console.log(e.detail.value);
    this.setData({
      checkboxChange:e.detail.value,
    })
  },
  jobCatagory(e){
    console.log(e);
      this.setData({
        index:e.detail.value
      })
  },
})