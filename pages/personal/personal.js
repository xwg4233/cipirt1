// pages/personal/personal.js
var QR = require("../../qrcode.js");
const api = require('../../api.js');
Page({
  /**
   * 页面的初始数据
   */
  data: {
    index:0,
    userInfo:"",
    src:'',
    nickName:'',
    showid: [{ id:'canvasId0'}],
    codeImg:'',
    
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var thas = this;
    wx.showLoading({
      title: '加载中...',
    })
    wx.getUserInfo({
      success: (res) => {
    
        console.log(res);
        var userInfo = res.userInfo
        var nickName = userInfo.nickName;
        var avatarUrl = userInfo.avatarUrl;
        wx.showLoading({
          title: '加载中...',
        })
        wx.downloadFile({
          url: avatarUrl,
          success: function (res) {
            
            wx.getImageInfo({
              src: '/img/invitationBg.jpeg',
              success: function (res) {
                wx.hideLoading()
                thas.setData({
                  canvaswidth: res.width,
                  canvasheight: res.height
                })
              },fail(e){
                wx.showToast({
                  title:e.msg,
                })

              }
            })
            thas.setData({
              userInfo: nickName,
              nickName: res.tempFilePath,
            })
          },fail(e){
            wx.showToast({
              title: e.msg,
            })
            
          }
        })
      },fail(res){
        wx.showToast({
          title: res.msg,
        })
      }
    })
   
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
 

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  generateinvitation(){
      wx.showLoading({
        title: '生成邀请函中..',
      })
    var thas = this;
    wx.request({
      url: api.wxCreateQRcode(),
      data:{
        Openid: wx.getStorageSync('openId')
      },
      method:'GET',
      success:function(e){
    
        
        wx.downloadFile({
          url: e.data,
          success: function (res) {
            console.log(res.tempFilePath);
            thas.setData({
              codeImg:res.tempFilePath,
            })
            thas.imgonload();
          
          }
        })
      
      },fail(res){
        wx.showToast({
          title: res.msg,
        })
      }
    })
  


  },



  inputVaule(e){
    console.log(e);
this.setData({
  userInfo: e.detail.value,
})
    
  },
  imgonload() {


    var that = this;
    console.log(that.data.showid);
    console.log(that.data.index);
    that.setData({
      canvasHidden: false,
    })
    if (!that.data.userInfo) {
      wx.showToast({
        title: '请输入名称',
        icon: 'none',
      })
      return
    }
    wx.getImageInfo({
      src: '/img/invitationBg.jpeg',
      success: function (res) {
        console.log(res)
        var ctx = wx.createContext()
        ctx.draw();
        ctx.drawImage('/img/invitationBg.jpeg', 0, 0, res.width, res.height);
        //二维码宽
        var codeWidth = 190;
        //二维码高
        var codeHieht = 190;
        //头像大小
        var gardenZise = 140;
        ctx.drawImage(that.data.codeImg, (res.width - codeWidth) / 2, (res.height - codeHieht) - 290, codeWidth, codeHieht);
        ctx.setFontSize(32)
        ctx.font = 'normal bold 32px sans-serif';
        ctx.setTextAlign('center')
        ctx.fillText(that.data.userInfo, res.width / 2, res.height / 2)
        ctx.beginPath();
        ctx.arc(res.width / 2, res.height / 2 - gardenZise / 2 - 60, gardenZise / 2, 0, Math.PI * 2, false)
        ctx.setStrokeStyle("#fff")
        ctx.clip();
        ctx.drawImage(that.data.nickName, (res.width / 2) - gardenZise / 2, res.height / 2 - 60 - gardenZise, gardenZise, gardenZise);

        wx.drawCanvas({
          canvasId: that.data.showid[that.data.index].id,
          actions: ctx.getActions() // 获取绘图动作数组

        })
        ctx.draw(true, setTimeout(() => {
          wx.canvasToTempFilePath({
            x: 0,
            y: 0,
            width: res.width,
            height: res.height,
            destWidth: res.width,
            destHeight: res.height,
            canvasId: String(that.data.showid[that.data.index].id),
            success: function (res) {
              wx.hideLoading();
              console.log(res.tempFilePath)
              var show = that.data.showid;

              var item = { 'id': 'canvasId' + Number(that.data.index + 1) };
              that.data.showid.push(item)
              that.setData({
                showid: that.data.showid,
                index: Number(that.data.index + 1),
              })
              wx.navigateTo({
                url: '../savecode/savecode?img=' + res.tempFilePath,
              })



            },
            fail: function (res) {
              console.log(res)
              wx.showToast({
                title: res,
              })
            }
          })

        }, 1000)
        );
        // ctx.draw(false);
      }
    })
  },
  navList() {
    if (wx.getStorageSync('code') == false) {
      app.openId();
    } else {
      wx.navigateTo({
        url: "../ticket/ticket",
      })
    }
  }
})