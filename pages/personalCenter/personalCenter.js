// pages/personalCenter/personalCenter.js
const api = require('../../api.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    show:1,
    silt:[
      {
        ico:'/img/coupons.png',
        name:"电子入场劵",
      },
      {
        ico:"/img/invitations.png",
        name:"生成邀请函"
      },
      {
        ico: "/img/collected.png",
        name: "已收藏展商"
      },
      {
        ico: "/img/collecteds.png",
        name: "已收藏会议"
      },
      {
        ico: "/img/personalActivities.png",
        name: "会议与活动"
      },
      {
        ico: "/img/feedback.png",
        name: "意见反馈区"
      },
      

    ],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  siltNav(e){
    console.log(e.currentTarget.dataset.index);
    switch (e.currentTarget.dataset.index){
      case 0:
        wx.navigateTo({
          url: '../ticket/ticket',
      })
    
      break;
      case 1:
        wx.navigateTo({
      url: '../invitation/invitation',
    })
      break;
      case 2:
      wx.navigateTo({
        url: '../exhibits/exhibits?openid=1',
      })
      break;
      case 3:
      wx.navigateTo({
        url: '../meetingSilt/meetingSilt',
      })
        break;
     case 4:
     wx.navigateTo({
       url:'../activity/activity',
     })
     break;

  


      break;
      case 5:
      wx.navigateTo({
        url: '../feedback/feedback',
      })

      break;

      }

  },
  userInformation(e){
console.log(e);
    if (e.detail.errMsg !='getPhoneNumber:ok'){

      
return
}
    wx.getUserInfo({
      success: function (res) {
        if (res['getUserInfo']) {
        }
        console.log(res);
        var userInfo = res.userInfo
        var nickName = userInfo.nickName
        var avatarUrl = userInfo.avatarUrl
        var gender = userInfo.gender //性别 0：未知、1：男、2：女
        var province = userInfo.province
        var city = userInfo.city
        var country = userInfo.country
        thas.setData({
          userInfo: userInfo
        })
      }, fail: function (res) {
        console.log(res);

      }
    })

  }
})