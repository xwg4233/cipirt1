// pages/activity/activity.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    listContent: [
      {
        content: "收费会议",
      },
      {
        content: "免费会议",
      }

    ],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  navConent(e){
    console.log(e);
    console.log(e.currentTarget.dataset.index)
    var id='';
    if (e.currentTarget.dataset.index==0){
      id=0
    }else{
      id=1
    }
    wx.navigateTo({
      url: '../meetingSilt/meetingSilt?id='+id,
    })
  },
  navList() {
    if (wx.getStorageSync('code') == false) {
      app.openId();
    } else {
      wx.navigateTo({
        url: "../ticket/ticket",
      })
    }
  }
})