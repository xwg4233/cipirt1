// pages/positioning/positioning.js
const app =getApp();
const api = require('../../api.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    plane:[

{
  lime:"一号馆平面图",
        img:"https://edentech.corpit.com.cn/wxfile/hall1.jpg"

},{
        lime:"二号馆平面图",
        img:"https://edentech.corpit.com.cn/wxfile/hall2.jpg"
}
    ],
    attr: [{
      ico: "/img/aboutPavilion.png",
      icoChose:'/img/aboutPavilionChose.png',
      text: "关于展馆",

    }, {
        ico: "/img/boothFigure.png",
      text: "展位图",
        icoChose:'/img/boothFigureChose.png',
    },
      {
        ico: "/img/meetingDistribution.png",
        icoChose: '/img/meetingDistributionChose.png',
        text: "会议分布",


      },
    ],
    ifShow: 1,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    app.imgshowloding();
    var thas=this;
    wx.request({
      url: api.getTypeDetails(),
      data: {
        Key: '15B4B190-4CE4-4885-A7E8-AF5CB8727A9C',
        TypeId: '2',

      },
      success(res) {
        console.log(res);
        thas.data.plane[0].img = res.data[1].DetailsImg;
        thas.data.plane[1].img = res.data[2].DetailsImg;   
        console.log(thas.data.plane);
        thas.setData({
          imgUrl: res.data,
          plane: thas.data.plane

        })
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
  indexNav(e) {
    console.log(e.currentTarget.dataset.index);
    this.setData({
      ifShow: e.currentTarget.dataset.index,
    })
var silt=""
    if (e.currentTarget.dataset.index==0){
      silt="关于展馆";
    }else if (e.currentTarget.dataset.index == 1){
      silt = "展位图";
    }else{
      silt = "会议分布";
    }
    wx.setNavigationBarTitle({
      title:silt
    })

  },
  previewImg(e){
    wx.previewImage({
      current: e.currentTarget.dataset.img, // 当前显示图片的http链接
      urls: [e.currentTarget.dataset.img] // 需要预览的图片http链接列表
    })
  },
  bindload(){
    app.deleloading();
  },
  navList() {
    if (wx.getStorageSync('code') == false) {
      app.openId();
    } else {
      wx.navigateTo({
        url: "../ticket/ticket",
      })
    }
  }
  
})