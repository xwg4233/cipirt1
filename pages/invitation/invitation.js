// pages/invitation/invitation.js
const app =getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    listContent:[
        {
        content:"展商（包括公司名与展位号）",
        },
        {
          content:"个人（包括微信头像与名称）",
        }

    ],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // this.setData({
    //   placeholder:"请输入"+this.data.exhibitors[index].name
    // })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  navConent(e){
    console.log(e.target.dataset.index);
    switch (e.target.dataset.index){
      case 0:
        if (wx.getStorageSync('code') == false) {
          app.openId();
          return
        }
      wx.navigateTo({
        url: '../exhibitorsInvitation/exhibitorsInvitation',
      })

      break;
  

    }

  },
  onGotUserInfo(e){
    if (e.detail.errMsg =="getUserInfo:ok"){
      if (wx.getStorageSync('code') == false) {
        app.openId();
        return
      }
      if (e.target.dataset.index==1){
        wx.navigateTo({
          url: '../personal/personal',
        })
        return
      }
      wx.navigateTo({
        url: '../exhibitorsInvitation/exhibitorsInvitation',
      })
    }
  },
  navList() {
    if (wx.getStorageSync('code') == false) {
      app.openId();
    } else {
      wx.navigateTo({
        url: "../ticket/ticket",
      })
    }
  }
})