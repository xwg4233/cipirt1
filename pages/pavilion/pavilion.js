
const app = getApp();
const api = require('../../api.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    national:[
        {
        icon:"https://edentech.corpit.com.cn/wxfile/德国.png",
        name:"德国国家展团"
        },
        {
          icon: "https://edentech.corpit.com.cn/wxfile/美国.png",
          name: "美国国家展团"
        },{
          icon: "https://edentech.corpit.com.cn/wxfile/韩国.png",
          name: "韩国国家展团"
        },
      {
        icon: "https://edentech.corpit.com.cn/wxfile/意大利.png",
        name: "意大利国家展团"
      },
      {
        icon: "https://edentech.corpit.com.cn/wxfile/中国.png",
        name: "中国台湾展团"
      },
      {
        icon: "https://edentech.corpit.com.cn/wxfile/中国.png",
        name: "中国佛山展团",
      }

    ],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    app.imgshowloding();
    var thas=this;
      wx.request({
        url: api.getPavilion(),
        data:{
          Key:'15B4B190-4CE4-4885-A7E8-AF5CB8727A9C',
        },
        success(res){
          wx.hideLoading()
            console.log(res);
              thas.setData({
                national: res.data
              })
        },fail(){
          wx.hideLoading()
        }
      })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  introduce(e){
    console.log(e)
    wx.navigateTo({
      url: '../introduce/introduce?index=' + e.currentTarget.dataset.item.PavilionId,
    })

  },
  navList() {
    if (wx.getStorageSync('code') == false) {
      app.openId();
    } else {
      wx.navigateTo({
        url: "../ticket/ticket",
      })
    }
  }
})