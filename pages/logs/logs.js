//logs.js
const util = require('../../utils/util.js')
const api =require('../../api.js')

Page({
  data: {
    code:0,
    logs: [],
    obtain:false,
    obtainContent:"获取验证码",
    coner:60,
    phone:'',
    show:0,
    ifshow:0,
  },
  onLoad: function () {
    this.setData({
      logs: (wx.getStorageSync('logs') || []).map(log => {
        return util.formatTime(new Date(log))
      })
    })
  },
  code(e){
    var thas=this;
    var myreg = /^[1][3,4,5,7,8][0-9]{9}$/;
    console.log(this.data.phone);
    if (this.data.code==1){
    
    return
    }
    if (!myreg.test(this.data.phone)) {
      wx.showToast({
        title: '请输入正确手机号',
        icon:'none'
      })
      return;

    } 
    this.setData({
      obtainContent: "秒后获取验证码",
      show:1,
      code: 1,
      obtain:true,
    })
    var interval= setInterval(()=>{
      if (this.data.coner==0){
        this.setData({
          show:0,
          obtainContent: "获取验证码",
          obtain: false,
          coner:60,
          code:0,
        })
        clearInterval(interval);
      }
      this.setData({
        coner:this.data.coner-1,
      })
    },1000)
    this.getVerificationCode()

  },
  getVerificationCode(){
    var thas=this;
    wx.request({
      url: api.getUseryzm(),
      data: {
        'Mobilephone': thas.data.phone,
        'Key': '15B4B190-4CE4-4885-A7E8-AF5CB8727A9C',
      },
      success(res) {
        console.log(res);
        wx.showToast({
          title: res.data.msg,
          icon: 'none',
        })

      },
    })
  },
  phone(e){
    console.log(e.detail.value);
    this.setData({
      phone: e.detail.value
    })
  },
  formValue(e){
    if (this.data.ifshow==1){
      wx.showToast({
        title: '需同意隐私协议',
        icon:'none'
      })
      return;

    }
    var thas=this;
    console.log(thas.data.phone);
      console.log(e.detail.value);
    if (!thas.data.phone){
      wx.showToast({
        title: '请输入手机号',
        icon: 'none'
      })
      return
    }
    var myreg = /^[1][3,4,5,7,8][0-9]{9}$/;
    if (!myreg.test(thas.data.phone)) {
      wx.showToast({
        title: '请输入正确手机号',
        icon: 'none'
      })
      return;

    } 
    if (e.detail.value.code == "" || e.detail.value.code == null || e.detail.value.code==undefined){
      wx.showToast({
        title: '请输入验证码',
         icon:'none'
      })
      return
    }
    wx.showLoading({
      title: '提交中...',
    })
      wx.request({
        url: api.getLogin(),
   
        data: {
          Mobilephone: thas.data.phone,
          Yzm: e.detail.value.code,
          Key: "15B4B190-4CE4-4885-A7E8-AF5CB8727A9C",
          openid: wx.getStorageSync('openId'),
        },

        success(e){
          wx.hideLoading()
          console.log(e)
          if(e.data.code==0){
            wx.showToast({
              title:e.data.msg,
              icon:'none',
            })
            return
          }
          wx.navigateTo({
            url: '../InformationTable/InformationTable?phone=' + thas.data.phone,
          })
        },fail(){
          wx.hideLoading()

        }
      })
  },

  onUnload: function () {
    wx.reLaunch({
      url: '../index/index'
    })
  },
  imgshow(){
    var ifshow = this.data.ifshow;
    var show=0;
    if (ifshow==0){
      show=1
    }else{
   show=0
    }
    this.setData({
      ifshow:show,
    })
  }
})
