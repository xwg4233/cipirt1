// pages/exhibits/exhibits.js
const api = require('../../api.js');

Page({

  /**
   * 页面的初始数据
   */

  data: {
    index: 0,
    indexconer: 10,
    cateNo: "",
    searchwords: "",
    searchwordssilt: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var thas = this;
    options.id
    if (options.id==0){
      wx.setNavigationBarTitle({
        title: "收费会议"
      })
    }else if (options.id == 1){
      wx.setNavigationBarTitle({
        title: "免费会议"
      })
    }else{
      wx.setNavigationBarTitle({
        title: "已收藏会议"
      })
    }
    this.wxelist();
    let query = wx.createSelectorQuery()
    wx.getSystemInfo({
      success: res => {
        query.selectAll('.nav-mian').boundingClientRect(rect => {
          let heightAll = 0;
          rect.map((currentValue, index, arr) => {
            heightAll = heightAll + currentValue.height
            console.log(res.windowHeight)
            console.log(heightAll)
            console.log(res.windowHeight - heightAll);

          })
          this.setData({
            scrollheight: res.windowHeight - heightAll
          })

        }).exec();

      }
    })

  },
  wxelist() {
    var thas = this;
    wx.showLoading({
      title: '加载中...',
    })
    wx.request({
      url: api.categoryDetails(),
      data: {

        'Key': "FF648EE9-C614-4CD8-A088-8401F808337C",
      },
      method: "GET",
      success(res) {
        console.log(res);
       wx.hideLoading()
        var list = [];
        if (res.data.length < thas.data.indexconer){
          console.log("小于")
          for (var i = 0; i < res.data.length; i++) {
            list.push(res.data[i]);
            console.log(i);
          }
        }else{
          for (var i = 0; i < thas.data.indexconer; i++) {
            list.push(res.data[i]);
          }
        }
        console.log(list);
       

        thas.setData({
          searchwordssilt: res.data,
          siltScroll: [],
        })
      },fail(res){
        wx.hideLoading();
        wx.showModal({
          title: '提示',
          content: res.msg,
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },
  searchChoose(e) {
    this.setData({
      index: e.detail.value,
      cateNo: this.data.species[e.detail.value].CateNo,
      indexconer: 10,

    })
    this.wxelist();


  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  bindscrolltolower: function (e) {
    console.log(e);
    var thas = this;
    console.log(thas.data.searchwordssilt[this.data.indexconer]);
    if (!thas.data.searchwordssilt[this.data.indexconer]){
      wx.showToast({
        title: '没有更多数据',
        icon:'none',
      })
      return;
    }


    for (var i = Number(this.data.indexconer); i < Number((10 + this.data.indexconer)); i++) {
      console.log(thas.data.searchwordssilt)
      console.log(i);
      if (thas.data.searchwordssilt.length-1 < Number(i)) {
        console.log(thas.data.siltScroll);
        this.setData({
          siltScroll: thas.data.siltScroll,
          indexconer: i
        })
        return

      } else {
        thas.data.siltScroll.push(this.data.searchwordssilt[i]);

      }



    }
    this.setData({
      siltScroll: thas.data.siltScroll,
      indexconer: i
    })



  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  bindinputInput: function (e) {
    console.log(e.detail.value);
    this.setData({
      searchwords: e.detail.value
    })
    this.wxelist();

  },
  details(e){
    console.log(e)
    wx.navigateTo({
      url: '../meetingDetails/meetingDetails',
    })

  },
})