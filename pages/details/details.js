// pages/details/details.js
const api = require('../../api.js');
const animation = wx.createAnimation({
  duration: 5000,
  delay: 300,
  timingFunction: 'linear',
})
Page({

  /**
   * 页面的初始数据
   */
  data: {
    animationData:{},
    appointment:0,
    index:0,
    indextime:0,
    date:[
      {time:'10-30'},
      { time: '10-31' },
      { time: '11-01' },
      { time: '11-02' },
    ],
    time:[
      {time:'9:30-10:30'},
      { time: '10:30-11:30' },
      { time: '11:30-12:30' },
      { time: '12:30-13:30' },
    ],
    shouchuang: 1, eid:'',
    name:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
 var thas=this;
 thas.setData({
   eid: options.eid
 })
 wx.request({
   url: api.getOpenIdCollection(),
   data: {
     OpenId:wx.getStorageSync('openId'),
     eid: options.eid,
     Key:'15B4B190-4CE4-4885-A7E8-AF5CB8727A9C',
   },
   success(e){
     console.log(e);
     thas.setData({
       shouchuang:e.data[0].Code
     })
   }
 })
    wx.request({
      url: api.wxEshow(),
      data: {
        eid: options.eid,
      },
      method: "GET",
      success(res) {
        console.log(res.data[0]);
        thas.setData({
          name: res.data[0]
        })
    
      }
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  shouchuang(){
    var chuang=0;
    if (this.data.shouchuang==2){
      chuang=1
      this.addshouchuang()
    }else{
      this.deleshouchuang();
      chuang=2
    }
    this.setData({
      shouchuang: chuang,
    })
  },

  addshouchuang(){
    var thas=this;
    wx.request({
      url: api.addCollectionList(),
      data:{
        OpenId: wx.getStorageSync('openId'),
        'Key': '15B4B190-4CE4-4885-A7E8-AF5CB8727A9C',
        Eid:thas.data.eid,
        ExhibitorName: thas.data.name.ECompanyCn,
      },
      success(e){
        console.log(e);
        wx.showToast({
          title: e.data[0].msg,
        })
  
      }
    })
  },
  deleshouchuang(){
    
    var thas = this;
    wx.request({
      url: api.delCollectionList(),
      data: {
        OpenId: wx.getStorageSync('openId'),
        'Key': '15B4B190-4CE4-4885-A7E8-AF5CB8727A9C',
        Eid: thas.data.eid,
      },
      success(e) {
        console.log(e);
        wx.showToast({
          title: e.data[0].msg,
        })
      }
    })
  },
  yunlan(e){
    wx.previewImage({
      current: 'https://edentech.corpit.com.cn/wxfile/hall1.jpg', // 当前显示图片的http链接
      urls: ['https://edentech.corpit.com.cn/wxfile/hall1.jpg'] // 需要预览的图片http链接列表
    })

  },
  appointment(e){
    console.log(e);
    animation.translate(0, -270).step();
    this.setData({
      appointment: 1,
        animationData: animation.export(),
    }); 
  },
  dateconer(e){
      console.log(e.detail.value);
      this.setData({
        index: e.detail.value,
      })
  },
  hiddenAppointment(){
    animation.translateY(0,0).step();
    this.setData({
      appointment: 0,
      animationData: animation.export(),
    }); 
  },
  time(e){
    this.setData({
      indextime: e.detail.value,
    })
  },
  textConer: function (e) {
this.setData({
  appointment:2,
});


  }
})
