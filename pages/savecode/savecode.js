// pages/savecode/savecode.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    show:0,
    imgconer:'保存图片'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    this.setData({
      img: options.img
    })

  },
  baochu(e){
    var thas=this
    wx.getSetting({
      success(res) {
        console.log(res);
        if (!res.authSetting['scope.writePhotosAlbum']) {
          wx.authorize({
            scope: 'scope.writePhotosAlbum',
            success() {
            wx.saveImageToPhotosAlbum({
              filePath: thas.data.img,
              success(res) { }
            })
            },fail(e){

           thas.setData({
             imgconer:'保存图片(去授权)',
             show:1,
           })
            }
          })
        }else{
          wx.saveImageToPhotosAlbum({
            filePath: thas.data.img,
            success(res) {

              wx.showToast({
                title: '保存成功',
              })
             }
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var thas = this;
    wx.getSetting({
      success(res) {
        console.log(res);
        if (!res.authSetting['scope.writePhotosAlbum']) {
          wx.authorize({
            scope: 'scope.writePhotosAlbum',
            success() {      
            }, fail(e) {
              thas.setData({
                imgconer: '保存图片(去授权)',
                show: 1,
              })
            }
          })
        }else{
          if (thas.data.show==1){
            wx.saveImageToPhotosAlbum({
              filePath: thas.data.img,
              success(res) { 
                wx.showToast({
                  title: '保存成功',
                })
              }
            })
          }
        
          thas.setData({
            imgconer: '保存图片',
            show: 0,
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})