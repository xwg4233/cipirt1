// pages/feedback/feedback.js
const api = require('../../api.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    add:[],
    feedback:[
      { id:0, name: "上海口腔展", show: true,},
      { id: 1,name: "产品", show: true, },
      { id: 2,name: "展商", show: true, },
      { id: 3,name: "活动", show: true, },
      { id: 4,name: "电子入场劵", show: true, },
      { id: 5,name: "课程", show: true,},
      { id: 6,name: "小程序功能体验", show: true,},
      { id: 7,name: "观众预登记", show: true,},
      { id: 8,name: "商旅服务", show: true,},
      { id: 9,name: "会议", show: true,},
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      wx.request({
        url: api.getOpinionType(),
        data:{
          Key:'15B4B190-4CE4-4885-A7E8-AF5CB8727A9C'
        },
        success: ((res)=>{
            console.log(res);
    
          var list=[];
          var listconer= { 'show': true};
          var i=0
            for(;i<res.data.length;i++){
          
              let timecolor = { data:res.data[i],'show': true};
              console.log(list.push(timecolor));
            }
          console.log(res.data);
          console.log(list);
            this.setData({
              feedback: list,
            })
        })

        
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  showColor(e){
    var add = this.data.add;
    console.log(e.currentTarget.dataset.item);
    if (e.currentTarget.dataset.item.show == true) {
      this.data.add.push(e.currentTarget.dataset.item.data.Id)
    } else {
      for (var i = 0; i < add.length; i++) {
        console.log([i])
        console.log(add[i])
        if (add[i] == 
          e.currentTarget.dataset.item.data.Id) {
          add.splice([i], 1);
        
        }

      }

    }
    var feedback = !this.data.feedback[e.currentTarget.dataset.index].show;
    let show = 'feedback[' + e.currentTarget.dataset.index +'].show';
    console.log(add);
    this.setData({
      [show]: feedback,
      add: this.data.add,
    })

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },


  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  submit(e){
console.log(e.detail.value.text);
console.log(this.data.add);
var that=this;
    if (this.data.add == "" || this.data.add == undefined || this.data.add==null) {
      wx.showToast({
        title: '请选择问题类型',
        icon: "none"
      })
      return
    }
if(!e.detail.value.text){
  wx.showToast({
    title: '请发表您的建议',
    icon:"none"
  })
  return
}
  

    wx.showLoading({
      title: '提交中...',
    })
    wx.request({
      url: api.addOpinionList(),
      data: {
        OpenId: wx.getStorageSync('openId'),
        Key:'15B4B190-4CE4-4885-A7E8-AF5CB8727A9C',
        'OpinionType': that.data.add,
        'Opinions': e.detail.value.text,

      },
      header: {},
      method: 'GET',
      dataType: 'json',
      responseType: 'text',
      success: function(res) {
        wx.hideLoading()
        console.log(res);
        wx.showToast({
          title: "感谢你的支持和反馈意见",
          icon:'none'
        })
        if(res.data[0].Code==1){
          setTimeout(()=>{
            wx.navigateBack({
            })
          },1000)
        
        }
      },
      fail: function(res) {
        wx.hideLoading();
        wx.showToast({
          title: res.data[0].msg,
          icon:'none'
        })
      },
      complete: function(res) {},
    })
  },

})