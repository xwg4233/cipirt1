// pages/information/information.js
var app=getApp()
const api = require('../../api.js');
Page({
  /**
   * 页面的初始数据
   */
  data: {
    imgShow:0,
    attr: [{
      ico:"/img/exhibitionInformation.png",
      icoChose:"/img/exhibitionInformationChose.png",
      text: "展会信息",

    }, {
        ico: "/img/previousReviews.png",
        icoChose: "/img/previousReviewsChose.png",
      text: "历届回顾",


    }],
    ifShow: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    app.imgshowloding();
    var thas=this;
    wx.request({
      url: api.getTypeDetails(),
      data:{
        Key:'15B4B190-4CE4-4885-A7E8-AF5CB8727A9C',
        TypeId:'1',
    
      },
      success(res) {
        console.log(res);
        thas.setData({
          imgUrl:res.data
        })
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  indexNav(e) {
    console.log(e.currentTarget.dataset.index);
    this.setData({
      ifShow: e.currentTarget.dataset.index,
    })
    if (e.currentTarget.dataset.index==1){
      wx.setNavigationBarTitle({
        title: '历史回顾'
      })
      return
    }
    wx.setNavigationBarTitle({
      title: '展会介绍'
    })


  },
  bindload(e){
    console.log(e);
    app.deleloading();
  },
  navList() {
    if (wx.getStorageSync('code') == false) {
      app.openId();
    } else {
      wx.navigateTo({
        url: "../ticket/ticket",
      })
    }
  },
  yunlanwendang(){
    wx.showLoading({
      title: '下载文档中.',
    })
    wx.downloadFile({
      url: 'https://edentech.corpit.com.cn/wxfile/DenTech%20China%202019%E4%B8%8A%E6%B5%B7%E5%8F%A3%E8%85%94%E5%B1%95_CN_%E5%B1%95%E5%90%8E%E6%8A%A5%E5%91%8A1202.pdf',
      success: function (res) {
        wx.showLoading({
          title: '正在打开文档',
        })
        const filePath = res.tempFilePath
        wx.openDocument({
          filePath: filePath,
          success: function (res) {
            wx.hideLoading();
          }
        })
      }
    })

  },
})