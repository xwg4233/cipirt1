// pages/entrance/entrance.js
const app =getApp();
const api = require('../../api.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    show:0,
    url:'',
    name:'尊敬的谢先生',
    content:'感谢您的注册登记第二十四届中国国际口腔器材展览会学术研究会',
    registration:'您的预登记号码为：2229074',
    registrationconer:"展会将于2020年10月28日至10月31日在上海世博观众预登记柜台领取胸卡入场参观或直接扫码入场。",
    registrationbottom:"我们期待与您的相见!",
    address:{
      lime:"时间：2020.10.28 - 2019.10.31",
      address:'上海世博展览馆（上海市浦东新区国展路1099号）,地铁8好像中华艺术攻战，耀华路，地铁7号线耀华路站，地铁13号线世博大道站）',
      signature:"DenTech China 2020组委会",

    }
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(wx.getStorageSync('openId'));
    var thas=this;
    wx.request({
      url: api.getOpenIdLogin(),
      data: {
        "OpenId": wx.getStorageSync('openId'),
        "Key": '15B4B190-4CE4-4885-A7E8-AF5CB8727A9C'
      },
      success(res) {
        console.log(res)
        var code = false;
        console.log(res.data.code);
        if (res.data.code == '0') {
          code = false;
return
        }
        thas.setData({
          data:res.data.data
        })

      }, fail() {

      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var thas=this;
    if (wx.getStorageSync('code') == false) {
      thas.setData({
        show: 1,
      })
    } else {
      thas.setData({
        show: 0,
      })
    }

  },
 
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  onTabItemTap: function (e) {
    app.openId();
    var thas=this;

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

   
})