// pages/myCollection/myCollection.js
const app = getApp();
const api = require('../../api.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    show:0,
    data:[],
    indexconer:10,
    attr:[{
      ico:"/img/collected.png",
      text:"已收藏展商",
      icoChose:'/img/collected-chose.png'
   
    },{
        ico:"/img/collecteds.png",
        text:"已收藏会议",
        icoChose: '/img/collecteds-chose.png'
     

    }],
    ifShow:0,

  },
  

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    app.openId();
    var thas = this;
    let query = wx.createSelectorQuery()
    wx.getSystemInfo({
      success: res => {
        query.selectAll('.mian').boundingClientRect(rect => {
          let heightAll = 0;
          rect.map((currentValue, index, arr) => {
            heightAll = heightAll + currentValue.height
            console.log(res.windowHeight)
            console.log(heightAll)
            console.log(res.windowHeight - heightAll);

          })
          this.setData({
            scrollheight: res.windowHeight - heightAll
          })

        }).exec();

      }
    })
  },
  wxelist() {
    var thas = this;
    wx.showLoading({
      title: '加载中...',
    })
    wx.request({
      url: api.getCollectionList(),
      data: { 
        OpenId: wx.getStorageSync('openId'),
        'Key': '15B4B190-4CE4-4885-A7E8-AF5CB8727A9C',
      },
      method: "GET",
      success(res) {
        console.log(res);
        wx.hideLoading()
        var list = [];
        if (res.data.length==0){
          thas.setData({
           show:1,
          })
          return
        }
        if (res.data.length < thas.data.indexconer) {
          for (var i = 0; i < res.data.length; i++) {
            list.push(res.data[i]);
            console.log(i);
          }
        } else {
          for (var i = 0; i < thas.data.indexconer; i++) {
            list.push(res.data[i]);
          }
        }
        console.log(list);
        thas.setData({
          searchwordssilt: res.data,
          siltScroll: list,
          show: 0,
        })
      }, fail(res) {
        wx.hideLoading();
        wx.showModal({
          title: '提示',
          content: res.msg,
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },
  searchChoose(e) {
    this.setData({
      index: e.detail.value,
      cateNo: this.data.species[e.detail.value].CateNo,
      indexconer: 10,

    })
    this.wxelist();


  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      ifShow: 0,
    })
    this.wxelist();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  bindscrolltolower: function (e) {
    console.log(e);
    var thas = this;
    console.log(thas.data.searchwordssilt[this.data.indexconer]);
    if (!thas.data.searchwordssilt[this.data.indexconer]) {
      wx.showToast({
        title: '没有更多数据',
        icon: 'none',
      })
      return;
    }


    for (var i = Number(this.data.indexconer); i < Number((10 + this.data.indexconer)); i++) {
      console.log(thas.data.searchwordssilt)
      console.log(i);
      if (thas.data.searchwordssilt.length - 1 < Number(i)) {
        console.log(thas.data.siltScroll);
        this.setData({
          siltScroll: thas.data.siltScroll,
          indexconer: i
        })
        return

      } else {
        thas.data.siltScroll.push(this.data.searchwordssilt[i]);

      }



    }
    this.setData({
      siltScroll: thas.data.siltScroll,
      indexconer: i
    })



  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  bindinputInput: function (e) {
    console.log(e.detail.value);
    this.setData({
      searchwords: e.detail.value
    })
    this.wxelist();

  },
  details(e) {
    console.log(e)
    wx.navigateTo({
      url: '../details/details?eid=' + e.currentTarget.dataset.item.EId,
    })

  },
  indexNav(e){
    console.log(e);
    this.setData({
     ifShow:e.currentTarget.dataset.index,
    })
    if (e.currentTarget.dataset.index==0){
      this.wxelist();
    }else{
      this.setData({
        show:1,
      })
    }


  },
  navList() {
    if (wx.getStorageSync('code') == false) {
      app.openId();
    } else {
      wx.navigateTo({
        url: "../ticket/ticket",
      })
    }
  },
  
})