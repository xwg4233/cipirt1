// pages/exhibits/exhibits.js
const api = require('../../api.js');
Page({
  /**
   * 页面的初始数据
   */
  data: {
    x: 0,
    y: 0,
    ifbg:0,
    tempFilePath: '',
    indexhuabu: 0,
    showid: [{ id: 'canvasId0' }],
    index:0,
    indexconer:10,
    cateNo:"",
    searchwords:"",
    searchwordssilt:[],
    speciesName:"产品种类",
    zhanshang:0,
    imgRes:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) { 
    var thas =this;
    wx.request({
      url: api.wxpcate(),
      data: {},
      success(res) {
      console.log(res);
        res.data.unshift(
          {
            CateNo: "",
            CateNameCn: "全部",
          }
        )
        console.log(res.data);
    thas.setData({
      species: res.data,
    })
      }
    })
    

    let query = wx.createSelectorQuery()
    var imgres='';
      wx.getSystemInfo({
            success: res => {
              query.selectAll('.nav-mian').boundingClientRect(rect => {
                  let heightAll = 0;
                     rect.map((currentValue, index, arr) => {
                        heightAll = heightAll + currentValue.height
                       imgres=res;
                   
                       this.setData({
                         scrollheight: res.windowHeight - heightAll,
                         imgRes: imgres
                       })
                  
          })
          
           
          
        }).exec();
        
      }
   })
    console.log(options);
    if (options.openid) {
      console.log("1")
      thas.setData({
        zhanshang:1,
      })
      thas.shouchuanglist();
      return
    } 
    this.wxelist();

  },
  wxelist(){
    var thas =this;
    wx.showLoading({
      title: '加载中...',
    })
    wx.request({
      url: api.wxelist(),
      data: {
        "CateNo": thas.data.cateNo,
        "Searchwords": thas.data.searchwords, 
      },
      method:"GET",
      success(res) {
        console.log(res);
        wx.hideLoading()
        var list=[];
        if (res.data.length < thas.data.indexconer) {
          for (var i = 0; i < res.data.length; i++) {
            list.push(res.data[i]);
            console.log(i);
          }
        } else {
          for (var i = 0; i < thas.data.indexconer; i++) {
            list.push(res.data[i]);
          }
        }

        thas.setData({
          searchwordssilt: res.data,
          siltScroll: list,
        })
      
      
      },fail(res){
        wx.hideLoading()
        wx.showToast({
          title: res.msg,
        })
      }
    })
  },
  shouchuanglist(){
    var thas = this;
    wx.request({
      url: api.getSearchCollectionList(),
      data: {
        'OpenId': wx.getStorageSync('openId'),
        "CateNo": thas.data.cateNo,
        "Searchwords": thas.data.searchwords,
        'Key':'15B4B190-4CE4-4885-A7E8-AF5CB8727A9C'
      },
      method: "GET",
      success(res) {
        console.log(res);
        var list = [];
        if (res.data.length < thas.data.indexconer) {
          for (var i = 0; i < res.data.length; i++) {
            list.push(res.data[i]);
            console.log(i);
          }
        } else {
          for (var i = 0; i < thas.data.indexconer; i++) {
            list.push(res.data[i]);
          }
        }

        thas.setData({
          searchwordssilt: res.data,
          siltScroll: list,
        })


      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },
  searchChoose(e){
    console.log(this.data.species[e.detail.value].CateNameCn)
    this.setData({
      index: e.detail.value,
      speciesName: this.data.species[e.detail.value].CateNameCn,
      cateNo: this.data.species[e.detail.value].CateNo,
      indexconer:10,

    })
    if (this.data.zhanshang==1){
      this.shouchuanglist()
      return
    }

    this.wxelist();


  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  bindscrolltolower:function(e){
    console.log(e);
    var thas = this;
    if (!thas.data.searchwordssilt[this.data.indexconer]) {
      wx.showToast({
        title: '没有更多数据',
        icon: 'none',
      })
      return;
    }
    for (var i = Number(this.data.indexconer); i < Number((10 + this.data.indexconer)); i++) {
      if (thas.data.searchwordssilt.length - 1 < Number(i)){

        this.setData({
          siltScroll: thas.data.siltScroll,
          indexconer:i
        })
        return

        }else{
        console.log(i);
        thas.data.siltScroll.push(this.data.searchwordssilt[i]);
        }
    }
    this.setData({
      siltScroll: thas.data.siltScroll,
      indexconer: i
    })



  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
   
  },
  bindinputInput:function(e){
    this.setData({
      searchwords:e.detail.value
    })
    if (this.data.zhanshang == 1) {
      this.shouchuanglist()
      return
    }
    this.wxelist();

  },
  slit(e){
    if (this.data.zhanshang == 1) {
      console.log(e.currentTarget.dataset.item)
      wx.navigateTo({
        url: '../details/details?eid=' + e.currentTarget.dataset.item.EId,
      })
      return
    }
    wx.navigateTo({
      url: '../details/details?eid=' + e.currentTarget.dataset.item.eid,
    })


  },
  yunlanImg(e){
    console.log(e);
    this.xiazai(e.currentTarget.dataset.item.eid);
  },
  

  xiazai(e) {
    var thas=this;
    wx.showLoading({
      title: '加载中..',
    })
    var imgRes = thas.data.imgRes;
    console.log(imgRes);
    wx.request({
      url: api.wxGetBoothMap(),
      data: { eid: e },
      success(e) {
        console.log(e)
        thas.setData({
          data: e.data[0],
          
        })
        wx.getImageInfo({
          src: e.data[0].FLUrl,
          success: function (res) {
            console.log(res.width);
            wx.hideLoading();
            var width= '';
            console.log(res.width / imgRes.screenWidth);
            console.log(thas.data.data.X);
            width = imgRes.screenWidth;
            thas.data.data.X = thas.data.data.X / (1000/imgRes.screenWidth)
            thas.data.data.Y = thas.data.data.Y / (1000 / imgRes.screenWidth)+0.3;
            thas.data.data.H = thas.data.data.H / (1000 / imgRes.screenWidth)
            thas.data.data.W = thas.data.data.W / (1000 / imgRes.screenWidth);
            thas.data.data.X = thas.data.data.X *2;
            thas.data.data.Y = thas.data.data.Y * 2;
            thas.data.data.H = thas.data.data.H * 2;
            thas.data.data.W = thas.data.data.W * 2;
            // var imgX = thas.data.data.X-66/2+5
            // var imgY = thas.data.data.Y-18;
          thas.setData({
           data: thas.data.data,
            ifbg:1,
          })
          },fail(res){
            wx.hideLoading()
            wx.showToast({
              title: res.data.msg,
            })

          }
        })  
      }
    })
   },
  guanbi(){
    this.setData({
      ifbg:0,
    })
  }
  
  
})