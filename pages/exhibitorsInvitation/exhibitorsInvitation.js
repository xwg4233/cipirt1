// pages/exhibitorsInvitation/exhibitorsInvitation.js
const api = require('../../api.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    boothno:'',
    name:'',
    canswidth:'',
    cansheight:'',
    tempFilePath:'',
    showid: [{ id: 'canvasId0' }],
    exhibitors:[
{name:"展商名"},
{name:"展位号"},
    ],
    indexhuabu:0,
    index:0,
    placeholder:'',
    content: {
      limeAnnotation: "注：请输入合同中完整展商名称或展位号生成邀请函。",
      limeContent: "“最具有影响力”活动需要生成次邀请函，活动详情请参考小程序首页最新活动。",

      contentlime: "最具有影响力 活动展商尊享礼遇",
      bottomContent: "（活动开始后每周公布排名一次，2019.10.10.公布最终排名，解释权归主办方）",
      content: [
        { content: '排名前十展商活动将获得：' },
        { content: '展会期间红包墙LOGO和现场鸣谢LOGO' },
        { content: '开展前官方微信产品介绍推广一次' },
        { content: '排名第一名展商将获得:(包括以上所有奖项)' },

        { content: '展会期间现场户外神秘广告，价值五万元！' },
        { content: '排名前三名展商将获得：(包括以上所有奖项)' },
        { content: '2020年度展会特轩插页广告一页（仅限当年参展商）' },
      



      ]
    },
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var thas=this;
    this.setData({
      placeholder: "请输入"+this.data.exhibitors[this.data.index].name,
    })
    wx.showLoading({
      title: '获取图片中..',
    })
    wx.getImageInfo({
      src: 'https://edentech.corpit.com.cn/wxfile/einvi.jpg',
      success: function (res) {
        console.log(res)
        var canswidth = res.width;
        var cansheight = res.height;
        thas.setData({
          canswidth: canswidth,
          cansheight: cansheight
        })
        wx.showLoading({
          title: '下载图片中..',
        })
        wx.downloadFile({
          url: 'https://edentech.corpit.com.cn/wxfile/einvi.jpg',
          success: function (res) {
            
            console.log(res);
            var tempFilePath = res.tempFilePath;
            console.log(tempFilePath)
            thas.setData({
              tempFilePath:tempFilePath
            })
            wx.hideLoading()
          },fail(){
            wx.showLoading({
              title: '下载失败请重试',
            })
          }
        })
      
      }

    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  },
  searchChoose(e){
      console.log(e.detail.value);
      this.setData({
        index: e.detail.value,
        placeholder: "请输入" + this.data.exhibitors[e.detail.value].name,
      })
  },
  bindinput(e){
    console.log(e.detail.value);
    this.setData({
      inputData: e.detail.value,
    })

  },
  formValidation(){
    var thas=this;
    console.log(this.data.inputData);
    if (!this.data.inputData){
   wx.showToast({
     title: '输入值无效',
   })
   return
    }
    wx.request({
      url: api.wxECheck(),
        data:{
          Keyword: thas.data.inputData,
        },
        success(e){
          console.log(e);
          if (!e.data[0].ECompanyCn || !e.data[0].boothno){
            wx.showToast({
              title: '输入值无效',
            })
            return
          }
          thas.setData({
            name:e.data[0].ECompanyCn,
            boothno:e.data[0].boothno,
          })
          thas.shengcheng();

        },

    })
    // 

  },
  

shengcheng(){
  wx.showLoading({
    title: '生成图片中..',
  })
  var that=this;
  console.log(that.data.canswidth);
  console.log(that.data.cansheight);
  var ctx = wx.createContext()
  ctx.draw();
  ctx.drawImage(that.data.tempFilePath, 0, 0, that.data.canswidth, that.data.cansheight);
  ctx.setFontSize(32)
  ctx.font = 'normal bold 32px sans-serif';
  ctx.setTextAlign('center')
  ctx.fillText(that.data.name, that.data.canswidth / 2, that.data.cansheight / 2+70)
  ctx.font = 'normal bold 42px sans-serif';
  ctx.setFillStyle('#fee100');
  ctx.fillText("欢迎莅临"+that.data.boothno+"展位", that.data.canswidth / 2, that.data.cansheight / 2+175)
  wx.drawCanvas({
    canvasId: that.data.showid[that.data.indexhuabu].id,
    actions: ctx.getActions() // 获取绘图动作数组

  })
  ctx.draw(true, setTimeout(() => {
    console.log(that.data.showid[that.data.indexhuabu].id);
    wx.canvasToTempFilePath({
      x: 0,
      y: 0,
      width: that.data.canswidth,
      height: that.data.cansheight,
      destWidth: that.data.canswidth,
      destHeight: that.data.cansheight,
      canvasId: String(that.data.showid[that.data.indexhuabu].id),
      success: function (res) {
        console.log(res.tempFilePath);
        wx.hideLoading();
        var show = that.data.showid;
        var item = { 'id': 'canvasId' + Number(that.data.indexhuabu + 1) };
        that.data.showid.push(item);
        wx.navigateTo({
          url: '../savecode/savecode?img=' + res.tempFilePath,
        })
        that.setData({
          showid: that.data.showid,
          indexhuabu: Number(that.data.indexhuabu + 1),
        })
      }

    })

  },1000)
  )
  }, 

  navList() {
    if (wx.getStorageSync('code') == false) {
      app.openId();
    } else {
      wx.navigateTo({
        url: "../ticket/ticket",
      })
    }
  }
})