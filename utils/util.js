const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}
//取倒计时（天时分秒）
function getTimeLeft(datetimeTo) {
  // 计算目标与现在时间差（毫秒）
  let time1 = new Date(datetimeTo).getTime();
  let time2 = new Date().getTime();
  let mss = time1 - time2;

  // 将时间差（毫秒）格式为：天时分秒
  let days = parseInt(mss / (1000 * 60 * 60 * 24));
  let hours = parseInt((mss % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  let minutes = parseInt((mss % (1000 * 60 * 60)) / (1000 * 60));
  let seconds = parseInt((mss % (1000 * 60)) / 1000);
  if (days < 0 && hours < 0 && minutes < 0 && seconds<0){
    days=0;
    hours=0;
    minutes=0;
    seconds=0;
  }


  return [
    {"time": days + "天"},
    {"time": hours + "时"},
    {"time": minutes + "分"},
    {"time": seconds + "秒"},
  ]
    
  
}
function dateTime(){
  var date=new Date()
  console.log(date)
  const year = date.getFullYear();
  const month = date.getMonth();
  const day = date.getDate();
  const getDay = date.getDay();

  var arrt=[1,2,3,4,5,6,7];
  var index='';
  var chinese=['一','二','三','四','五','六','七']
 for( var i=0;i<arrt.length;i++){
   if (Number(getDay) == arrt[i]){
      index=i;
   }
 };
  console.log(chinese[index]);
var dateTime=  year + "年" + (month+1)+ "月" + day + "日";
  var getDayTime = "星期" + chinese[index];
  return {
    'yearTime': String(dateTime),
    'getDayTime': getDayTime,
  }

}



module.exports = {
  formatTime: formatTime,
  getTimeLeft: getTimeLeft,
  dateTime: dateTime,

}
